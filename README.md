# Retrieve own YouTube comments

Pay attention to update `config.py` cookies with your own that you can grab by opening your `Web Developer Tool` `Network` tab (`Ctrl` + `Shift` + `E` on Firefox) filtering `HTML` and reaching https://myactivity.google.com/page?page=youtube_comments. You should observe your request to this URL and in the `Cookie` request header you should find the mentioned cookies.
