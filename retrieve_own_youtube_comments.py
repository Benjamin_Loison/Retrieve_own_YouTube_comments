#!/usr/bin/python3

import requests
import config
import re
import json

url = 'https://myactivity.google.com/page?page=youtube_comments'

cookies = {
    'SID': config.SID,
    'HSID': config.HSID,
    'SSID': config.SSID,
    'OSID': config.OSID,
    '__Secure-1PSIDTS': config.__Secure_1PSIDTS
}

# First page is made of 100 comments.

text = requests.get(url, cookies = cookies).text

data = json.loads(re.findall(', data:(.*?), sideChannel', text)[-1])

def treatData(data):
    items = data[0]
    for item in items:
        timestamp = item[4]
        commentId = item[5]
        comment = item[9][0]
        video = item[23]
        # Otherwise may be a community post comment for instance.
        if video is not None:
            videoDuration = video[1]
            videoId = video[3].split('=')[1]
            videoTitle = item[32][0][1]
            print(timestamp, commentId, comment, videoDuration, videoId, videoTitle)

paginationData = json.loads(re.search('window.WIZ_global_data = (.*?);</script>', text).group(1))

def getNextPageData(paginationToken):
    intermediary0Data = [
        [
            None,
            None,
            [
                'youtube_comments'
            ]
        ],
        paginationToken,
        100
    ]

    intermediary1Data = [
        [
            [
                'y3VFHd',
                json.dumps(intermediary0Data, separators=(',', ':')),
                None,
                'generic'
            ]
        ]
    ]

    nextPageData = f'f.req={json.dumps(intermediary1Data, separators=(",", ":"))}&at={paginationData["SNlM0e"]}'
    return nextPageData

def treatFollowingPage(data):
    url = 'https://myactivity.google.com/_/FootprintsMyactivityUi/data/batchexecute'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    nextPageData = getNextPageData(data[1])
    text = requests.post(url, cookies = cookies, headers = headers, data = nextPageData).text

    data = json.loads(json.loads(re.search('\[\["wrb.fr",(.*)\]\]', text).group())[0][2])

    treatData(data)
    return data

while len(data) >= 2:
    data = treatFollowingPage(data)
